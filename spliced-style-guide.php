<?php
/*
Plugin Name: Spliced Style Guide
Plugin URI: http://spliced.co
Description: Generates a style guide for a theme
Version: 0.1
Author: Dan Bissonnet, Spliced Digital
Author URI: http://spliced.co/
*/

/**
 * Copyright (c) 2012 Your Name. All rights reserved.
 *
 * Released under the GPL license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * This is an add-on for WordPress
 * http://wordpress.org/
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * **********************************************************************
 */
namespace Spliced\Plugin;

class Spliced_Style_Guide {
	static $_hooker;
	static $page_slug = 'style-guide';
	static $settings = array();

	function bootstrap( $hooker = null ) {
		try {
			if ( $hooker ) {
				if ( !method_exists( $hooker, 'hook' ) )
					throw new \BadMethodCallException( 'Class ' . get_class( $hooker ) . ' has no hook() method.', 1 );

				self::$_hooker = $hooker;
				self::$_hooker->hook( __CLASS__, 'thebp' );
			} else {
				throw new \BadMethodCallException( 'Hooking class for ' . __CLASS__ . ' not specified.' , 1 );
			}
		} catch ( Exception $e ) {
			wp_die( wp_get_theme() . ' theme bootstrap error: ' . $e->getMessage(),  wp_get_theme() . ' theme bootstrap error: ' );
		}

		register_activation_hook( __FILE__, array( __CLASS__, 'activation' ) );
	}

	function activation() {
		add_option( 'spliced-style-guide', array() );

		if ( !file_exists( get_stylesheet_directory() . '/style-guide' ) ) {
			wp_die( sprintf( 'Activation error. Please copy the contents of <code>%s</code> to <code>%s</code> to and reload this page to activate the plugin. Thank you.', __DIR__ . '/theme/', get_stylesheet_directory() ) , 'Activation Error' );
		}
	}

	function action_init () {
		self::$settings = array(
			'hide_menus' => false,
			'hide_widgets' => true,
		);
	}

	function action_template_redirect () {
		global $wp_query;

		if ( is_404() && get_query_var( 'name' ) === self::$page_slug ) {
			$wp_query->is_404 = false;
			$wp_query->is_page = true;

			if ( !file_exists( get_stylesheet_directory() . '/style-guide' ) )
				throw_error( __( 'Style Guide directory not found in theme. Maybe copy the default one from the plugin.', 'spliced-style-guide' ) );

			// Can override the main style guide template in the theme if you wish.
			$template = locate_template( array( 'style-guide.php' ), false, true );

			// Otherwise use the default
			if ( empty($template) ) $template  = __DIR__ . '/theme/style-guide.php';

			include_once $template;
			die();
		}
	}

	function filter_wp_nav_menu_args( $args ) {
		if ( self::$settings['hide_menus'] ) {
			return array();
		}

		return $args;
	}
}

/**
 * Bootstrap or die
 */
try {
	if ( class_exists( 'Bisso_Hooker' ) ) {
		Spliced_Style_Guide::bootstrap( new \Bisso_Hooker );
	} else {
		throw new \Exception( 'Class Bisso_Hooker not found. Check that the plugin is installed.', 1 );
	}
} catch ( Exception $e ) {
	wp_die( $e->getMessage(), $title = 'Theme Exception' );
}
