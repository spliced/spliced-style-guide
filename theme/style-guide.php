<?php
/**
 * Style Guide
 */
namespace Spliced\Theme\Chool;

get_header(); ?>
<style>
.pattern {
	text-align: right;
    clear: both;
    margin-bottom: 2em;
}

.pattern-title,
.pattern-subtitle {
	text-align: left;
	color: #ccc;
	border-bottom: 1px solid #eee;
	text-transform: capitalize;
	margin: 1em 0;
	padding-bottom: 0.5em
}

.pattern-subtitle {
	text-transform: none;
	font-size: 0.9em;
}

.pattern .display {
    width: 70%;
    float: left;
    text-align: left;
    margin-bottom: 1em;
}

.pattern .source {
	display: none;
    width: 30%;
    float: right;
    text-align: left;
    padding-left: 1em;
}

.pattern .source code {
	font-family: Source Code Pro, monospace;
	line-height: 1.4;
	width: 100%;
	overflow-y: auto;
	white-space: pre-wrap;
	display: block;
	margin-bottom: 1em;
}

.patterns-toc {
	-webkit-columns: 3;
	line-height: 1.4;
}
</style>
<div id="primary">
<header class="entry-header skin-gutter-below">
	<h1 class="entry-title heading-alpha">Style Guide</h1>
</header><!-- .entry-header -->

<?php $files = glob( __DIR__ . '/style-guide/*.{html,php}', GLOB_BRACE ); sort( $files ); ?>

<ul id="patterns-toc" class="patterns-toc">
<?php foreach ( $files as $key => $file ) :
	$nicename = preg_replace( '~\.php|\.html~', '',  basename( $file ) );
	$nicename = explode( '-', $nicename );
	$nicename = '<strong style="font-weight: bolder;">' . array_shift($nicename) . '</strong> ' . implode( ' ', $nicename );
	?>
	<li><a href='#<?php echo preg_replace('~\.html|\.php~', '', basename($file) ) ?>'><?php echo $nicename ?></a></li>
<?php endforeach ?>
</ul>

<?php foreach ( $files as $file ): ?>
    <div class="pattern" id="<?php echo preg_replace('~\.html|\.php~', '', basename($file) ) ?>">
    	<div class="pattern-title"><?php echo str_replace( '-', ' ', preg_replace('~\.html|\.php~', '', basename($file) ) ) ?></div>
	    <div class="display">
			<?php include( $file ) ?>
	    </div>
	    <div class="source copy-much-smaller">
<pre><code><?php
echo htmlspecialchars( file_get_contents( $file ) )
?></code></pre>
		    <p><a href="style-guide/<?php echo basename( $file ) ?>"><?php echo basename( $file ) ?></a></p>
	    </div>
	    <div style="clear:both"><a href="#patterns-toc">Top</a></div>
    </div>
<?php endforeach ?>
</div>
<?php get_footer() ?>